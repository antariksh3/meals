import React , {useState, useEffect, useCallback} from 'react';
import { StyleSheet, Text, View , Switch } from 'react-native';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import HeaderButton from '../components/HeaderButton';
import Colors from '../constants/colors';
import FilterSwitch from '../components/FilterSwitch';
import {useDispatch} from 'react-redux';
import {setFilters } from '../store/actions/meals';

const FiltersScreen = props => {

    const {navigation} = props;

    const [ isGlutenFree , setGlutenFree ] = useState(false);
    const [ isLactoseFree , setLactoseFree ] = useState(false);
    const [ isVegan , setVegan ] = useState(false);
    const [ isVeg , setVeg ] = useState(false);

    const dispatch = useDispatch();

    const saveFilters = useCallback(() => {
        //this func captures current state and makes it available to navigation options
        const appliedFilters = {
            glutenFree : isGlutenFree ,
            lactoseFree : isLactoseFree ,
            vegan : isVegan ,
            veg : isVeg
        }
        console.log("Applied Filters");
        console.log(appliedFilters);
        dispatch(setFilters(appliedFilters));
    }, [isGlutenFree, isLactoseFree, isVegan, isVeg, dispatch]);
    //the saveFilters callback will execute only when 1 of these 4 boolean updates
    //nice way to block un-necessary executions and save time

    useEffect(() => {
        props.navigation.setParams({save: saveFilters});
        //now param: 'save' will have data as saveFilters will be executed on reaching nav data
        //so params are used to communicate between components and navigation options
    }, [saveFilters])

    return (
        <View style={styles.screen}>
            <Text style={styles.title}>Apply Filters</Text>
            <FilterSwitch label={'Gluten-Free'} state={isGlutenFree} 
                onChange={newValue => setGlutenFree(newValue)}/>
            <FilterSwitch label={'Lactose-Free'} state={isLactoseFree} 
                onChange={newValue => setLactoseFree(newValue)}/>
            <FilterSwitch label={'Vegan'} state={isVegan} 
                onChange={newValue => setVegan(newValue)}/>
            <FilterSwitch label={'Vegetarian'} state={isVeg} 
                onChange={newValue => setVeg(newValue)}/>
        </View>
    );
};

FiltersScreen.navigationOptions = navData => {
    return {
        headerTitle : 'Filter Meals',
        headerLeft : () => 
            
            <HeaderButtons HeaderButtonComponent={HeaderButton} >
                <Item title="Menu" iconName="ios-menu" onPress={() => {
                    navData.navigation.toggleDrawer();
                }}  />
            </HeaderButtons>
        ,
        headerRight : () => 
            <HeaderButtons HeaderButtonComponent={HeaderButton} >
                <Item title="Save" iconName="ios-save" onPress={() => {
                    navData.navigation.getParam('save')();
                    //this () at the end executes the save method callback
                }}  />
            </HeaderButtons> 
        , 
        headerStyle : {
            backgroundColor : Colors.accentColor 
        }
    }
};

const styles = StyleSheet.create({
    screen : {
        flex: 1,
        alignItems : 'center',
    } , 
    title : {
        fontSize: 22,
        margin : 20,
        textAlign : 'center'
    } , 
    
});

export default FiltersScreen;
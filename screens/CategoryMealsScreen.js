import React from 'react';
import { StyleSheet, FlatList, Text, View, Button } from 'react-native';
import {CATEGORIES} from '../data/dummy-data'
import MealList from '../components/MealList';
import { useSelector} from 'react-redux'; 

const CategoryMealsScreen = props => {

    const catId = props.navigation.getParam('categoryId');

    const availableMeals = useSelector(state => state.meals.filteredMeals);
    //state will return the meals that respect the filters
    //in referrence with store/reducers/meals.js state = initialState.filteredMeals

    const selectedCategory = CATEGORIES.find(cat => cat.id === catId); 
    const displayedMeals = availableMeals.filter(
        meal => meal.categoryIds.indexOf(catId)>=0
    );
    if(displayedMeals.length === 0 ){
        return(
            <View style={styles.screen}>
                <Text>No meals to display</Text>
                <Text>(Tip : Check your Applied Filters)</Text>
            </View>
        );
    }
    return (
        <MealList listData={displayedMeals} navigation={props.navigation}></MealList>
    );
};

CategoryMealsScreen.navigationOptions = navigationData => {
    const catId = navigationData.navigation.getParam('categoryId');
    const selectedCategory = CATEGORIES.find(cat => cat.id === catId); 
    return {
        headerTitle : selectedCategory.title + " Meals"
    };
};



const styles = StyleSheet.create({
    screen : {
        flex: 1,
        alignItems : 'center',
        justifyContent : 'center'
    }
});

export default CategoryMealsScreen;
import React from 'react';
import { StyleSheet, FlatList, Text, View, Button, TouchableOpacity } from 'react-native';
import {CATEGORIES} from '../data/dummy-data';
import Colors from '../constants/colors';
import CategoryGridTitle from '../components/CategoryGridTitle';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import HeaderButton from '../components/HeaderButton';

const CategoriesScreen = props => {
    //console.log(props);
    const renderGridItems = (itemData) => {
        return (
                <CategoryGridTitle title={itemData.item.title}
                    color = {itemData.item.color}
                    onSelect = {() => {
                        props.navigation.navigate({routeName : 'Category Meals',
                        params: {
                                categoryId : itemData.item.id
                            }
                        })
                    }} 
                />
        );
    };
    return (
        <FlatList data={CATEGORIES} renderItem={renderGridItems} numColumns={2}>

        </FlatList>
    );
};

CategoriesScreen.navigationOptions = navData => {
    return {
        headerTitle : 'Categories Screen 🏠',
        headerLeft : () => 
            
                <HeaderButtons HeaderButtonComponent={HeaderButton} >
                    <Item title="Menu" iconName="ios-menu" onPress={() => {
                        navData.navigation.toggleDrawer();
                    }}  />
                </HeaderButtons>
            ,
        headerStyle : {
            backgroundColor : Colors.accentColor 
        }
    }
};

const styles = StyleSheet.create({
    screen : {
        
    },
    text : {
        textAlign : 'center'
    }
});

export default CategoriesScreen;
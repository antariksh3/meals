import React , {useEffect, useCallback} from 'react';
import { ScrollView, StyleSheet, Text, View, Image } from 'react-native';
//import { MEALS } from '../data/dummy-data';
import { HeaderButtons , Item } from 'react-navigation-header-buttons';
import HeaderButton from '../components/HeaderButton'
import {useSelector, useDispatch} from 'react-redux';
import {toggleFavorite} from '../store/actions/meals';

const MealDetailsScreen = props => {

    const availableMeals = useSelector(state => state.meals.meals);
    const mealId = props.navigation.getParam('mealId');
    const selectedMeal = availableMeals.find(meal => meal.id === mealId);
    const currMealIsFav = useSelector(state => 
        state.meals.favorites.some(meal => meal.id === mealId)
    );

    //console.log("img url: "+selectedMeal.imageUrl);

    const dispatch = useDispatch();

    const toggleFavHandler = useCallback(() => {    
        dispatch(toggleFavorite(mealId));
        console.log("toggleFavHandler called");
    }, [dispatch, mealId]);

    useEffect(() => {
        //props.navigation.setParams({mealTitle: selectedMeal.title});
        props.navigation.setParams({toggleFav: toggleFavHandler});
        console.log("use effect called");
    }, [toggleFavHandler]);

    useEffect(() => {
        props.navigation.setParams({isFav: currMealIsFav})
    }, [currMealIsFav]);
    // when currMealIsFav changes, new info will be passed to useEffect

    return (
    <ScrollView>
        <Image source={{uri: selectedMeal.imageUrl}} style={styles.imageStyle}/>
        <View style={styles.details}>
            <Text>{selectedMeal.duration} m</Text>
            <Text>Complexity: {selectedMeal.complexity}</Text>
            <Text>$$$: {selectedMeal.affordability}</Text>
        </View>
        <Text style={styles.title}>Ingredients</Text>
        {selectedMeal.ingredients.map(ingredient => 
            <Text style={{paddingTop : 5, paddingLeft: 10, paddingRight : 10}} 
                key={selectedMeal.ingredient}>
                {ingredient}
            </Text>)
        }
        <Text style={styles.title}>Steps</Text>
        {selectedMeal.steps.map(steps => 
            <Text style={{paddingTop : 5, paddingLeft: 10, paddingRight : 10}} 
                key={steps}>
                {steps}
            </Text>)
        }
    </ScrollView>
    );
};

MealDetailsScreen.navigationOptions = (navigationData) => {
    const mealTitle = navigationData.navigation.getParam('mealTitle');
    //const meal_id = navigationData.navigation.getParam('mealId');
    const toggleFavorite = navigationData.navigation.getParam('toggleFav');
    const isFav = navigationData.navigation.getParam('isFav');
    //const seleted_meal = availableMeals.find(meal => meal.id === meal_id)
    return {
        headerTitle: mealTitle ,
        headerRight : () => 
            <HeaderButtons HeaderButtonComponent = {HeaderButton}>
                <Item 
                    title='Favorite' 
                    iconName={isFav ? 'ios-star' : 'ios-star-outline'}
                    onPress={toggleFavorite}
                />
            </HeaderButtons>
    };
};

const styles = StyleSheet.create({
    screen : {
        flex: 1,
        alignItems : 'center',
        justifyContent : 'center'
    } , 
    title: {
        fontSize : 20 ,
        textAlign : 'center'
    } , 
    imageStyle : {
        width: '100%',
        height: 200,
         
    } , 
    details : {
        flexDirection : 'row',
        padding : 15 ,
        justifyContent : 'space-around'
    } ,

});

export default MealDetailsScreen;
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MealList from '../components/MealList';
import { MEALS } from '../data/dummy-data';
import { HeaderButtons , Item } from 'react-navigation-header-buttons';
import HeaderButton from '../components/HeaderButton';
import Colors from '../constants/colors';
import {useSelector} from 'react-redux'; 


const FavoritesScreen = props => {

    const availableMeals = useSelector(state => state.meals.meals);
    //first meals is the slice of state
    //second meals accesses meals from initialState from store/reducers/meals.js

    //LEGACY
    //const favMeals = MEALS.filter(meal => meal.id === 'm1' || meal.id === 'm2')

    const favMeals = useSelector(state => state.meals.favorites);
    if( !favMeals || favMeals.length === 0){
        return(
            <View style={styles.screen}>
                <Text>No Favorites. Start adding some.</Text>
            </View>
        );
    }
    return (
        <MealList listData={favMeals} navigation={props.navigation}></MealList>
    );
};

FavoritesScreen.navigationOptions = (navData) => {
    console.log('navData for Favorites Screen');
    console.log(navData.navigation.state.routeName);
    return {
        headerTitle : 'Your Favorites',
        headerStyle : {
            backgroundColor : Colors.accentColor 
        }
    };
};

const styles = StyleSheet.create({
    screen : {
        flex: 1,
        alignItems : 'center',
        justifyContent : 'center'
    }
});

export default FavoritesScreen;
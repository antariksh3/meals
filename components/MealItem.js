import React from 'react';
import { StyleSheet, FlatList, Text, View, Button, TouchableOpacity, 
    TouchableNativeFeedback, ImageBackground } from 'react-native';

const MealItem = props => {
    return (
        <View style={styles.mealItem}>
            <TouchableOpacity onPress={props.onSelectMeal}>
                <View>
                    <View style={{...styles.mealRow, ...styles.mealHeader}}>
                        <ImageBackground source={{uri: props.image}} style={styles.bgimage}>
                            <Text style={styles.title}>{props.title}</Text>
                        </ImageBackground>
                        
                    </View>
                    <View style={{...styles.mealRow, ...styles.mealDetails}}>
                        <Text>{props.duration} m</Text>
                        <Text>Complexity: {props.complexity}</Text>
                        <Text>$$$: {props.affordability}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    mealRow : {
        flexDirection : 'row',
        
    } , 
    mealItem  :{
        margin : 20,
        height : 200 ,
        width : '90%' ,
        backgroundColor : '#ccc',
        borderRadius : 10,
        overflow : 'hidden'
    } , 
    mealHeader : {
        height : '90%' ,
    } , 
    mealDetails : {
        paddingHorizontal : 10 ,
        justifyContent : 'space-between',
        paddingHorizontal : 10,
        justifyContent : 'space-between'
    } , 
    bgimage : {
        height : '100%',
        width : '100%',
        justifyContent : 'flex-end'
    } , 
    title : {
        fontFamily  : 'open-sans-bold',
        fontSize : 22 ,
        color : 'white',
        backgroundColor : 'rgba(0,0,0,0.5)',
        paddingVertical : 5 ,
        paddingHorizontal : 12
    }
});

export default MealItem; 
import React , {useState} from 'react';
import { StyleSheet, Text, View , Switch } from 'react-native';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import HeaderButton from '../components/HeaderButton';
import Colors from '../constants/colors';

const FilterSwitch = props => {
    return ( 
        <View style={styles.filterContainer}>
            <Text>{props.label}</Text>
            <Switch value={props.state} 
                onValueChange={props.onChange}
                trackColor={{true : 'red'}}
                thumbColor={'orange'}/>
        </View>
    );
}

const styles = StyleSheet.create({
    filterContainer : {
        flexDirection : 'row',
        justifyContent : 'space-between',
        alignItems: 'center',
        width : '80%',
        marginVertical : 10
    }
});

export default FilterSwitch;

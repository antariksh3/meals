import React from 'react';
import {TouchableOpacity, View, Text, StyleSheet, Platform, TouchableNativeFeedback} from 'react-native';

const CategoryGridTitle = props => {
    let TouchableCmp = TouchableOpacity;
    if(Platform.OS ===  'android'  && Platform.Version >= 21 ){
        TouchableCmp = TouchableNativeFeedback;
    }
    return (
        <View style={styles.gridItem}>
        <TouchableCmp style={styles.gridItem} onPress={props.onSelect}>
            <View style={{...styles.container, ...{backgroundColor : props.color}}}>
                <Text style={styles.title} numberOfLines={2}>{props.title} </Text>
            </View> 
        </TouchableCmp>
        </View>
    );
};

const styles = StyleSheet.create({
    gridItem : {
        justifyContent : 'center',
        borderRadius : 20,
        flex : 1,
        overflow : 'hidden',
        margin : 5 ,
        height : 160 ,
        elevation : 5
    } , text : {
        textAlign : 'center'
    } , container : {
        flex : 1 ,
        elevation : 3 ,
        borderRadius: 20 ,
        padding : 15,
        justifyContent : 'flex-end',
        alignItems : 'flex-end'
    } , title : {
        //fontFamily : 'open-sans-bold',
        fontSize : 20 ,
        textAlign : 'right'
    }
    
});

export default CategoryGridTitle ;
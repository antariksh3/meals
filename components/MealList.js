import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import MealItem from './MealItem';
const MealList = props => {

    const renderMealItem = itemData => {
        return (
            <MealItem 
                title={itemData.item.title} 
                duration={itemData.item.duration} 
                complexity={itemData.item.complexity}
                affordability={itemData.item.affordability}
                image={itemData.item.imageUrl}
                onSelectMeal={() => {
                    props.navigation.navigate({routeName : 'Meal Details', 
                    params: {
                        mealId : itemData.item.id ,
                        mealTitle : itemData.item.title
                    }});
                }}/>            
        )
    };

    return (
        <View style={styles.list}>
            <FlatList data={props.listData} keyExtractor={(item, index) => item.id}
                renderItem={renderMealItem} style={{width : '100%'}}>
            </FlatList>   
        </View>
    );
};

const styles = StyleSheet.create(
  {
    list : {
        flex :1 ,

    }
  } 
);

export default MealList;

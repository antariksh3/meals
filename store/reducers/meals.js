
import {MEALS} from '../../data/dummy-data';
import { TOGGLE_FAVORITE, SET_FILTERS } from '../actions/meals';

const intialState = {
    meals : MEALS,
    filteredMeals : MEALS,
    favorites : [] 
};

const mealsReducer = (state = intialState, action) => {
    switch(action.type){
        case TOGGLE_FAVORITE :
        
            const existingIndex = state.favorites.findIndex
                (meal => meal.id === action.mealId);
            if(existingIndex >= 0){
                const updatedFavMeals = [...state.favorites];
                updatedFavMeals.splice(existingIndex , 1);
                return {...state, favorites: updatedFavMeals}
            } else {
                //console.log("state");
                //console.log(state);
                //console.log(state.meals);
                const meal = state.meals.find(meal => meal.id === action.mealId)
                return {...state, favorites: state.favorites.concat(meal)};
            }

        case SET_FILTERS :
            const appliedFilters = action.filters;
            const updatedFilteredMeals = state.meals.filter(meal => {
                if(appliedFilters.glutenFree && !meal.glutenFree){
                    return false;
                }
                if(appliedFilters.lactoseFree && !meal.lactoseFree){
                    return false;
                }
                if(appliedFilters.veg && !meal.veg){
                    return false;
                }
                if(appliedFilters.vegan && !meal.vegan){
                    return false;
                }
                return true;
            });
            return {...state, filteredMeals: updatedFilteredMeals}
        default :
            return state;
    }
    return state;
}

export default mealsReducer;
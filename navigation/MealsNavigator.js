import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import CategoriesScreen from '../screens/CategoriesScreen';
import CategoryMealsScreen from '../screens/CategoryMealsScreen';
import MealDetailsScreen from '../screens/MealDetailsScreen';
import Colors from '../constants/colors';
import FavoritesScreen from '../screens/FavoritesScreen';
import { Ionicons } from '@expo/vector-icons';
import FiltersScreen from '../screens/FiltersScreen';
import {Platform} from 'react-native';

//this navigator will be nested in tabs navigator
const MealsNavigator = createStackNavigator({
    'Categories Screen': CategoriesScreen,
    'Category Meals': CategoryMealsScreen,
    'Meal Details': MealDetailsScreen,
    'Favorites Screen': FavoritesScreen
}, {
    defaultNavigationOptions: {
        headerStyle: Colors.primary,
    }
});

const FavoritesNavigator = createStackNavigator({
    Favorites: FavoritesScreen,
    'Meal Details': MealDetailsScreen
}, {
    defaultNavigationOptions: {
        headerStyle: Colors.primary,
    }
});

const MealsFavTabNavigator = createBottomTabNavigator({
    Meals: {
        screen: MealsNavigator, 
        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return tabInfo.focused ? 
                    <Ionicons name='ios-restaurant' size={32} color='orange' />
                    : <Ionicons name='ios-restaurant' size={32} color='grey' />;
            }
        }
    },
    Favorites: {
        screen: FavoritesScreen, 
        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return tabInfo.focused ?
                    <Ionicons name='ios-star' size={32} color='orange' />
                    : <Ionicons name='ios-star' size={32} color='grey' /> ;
            }
        }
    }
}, {
    tabBarOptions: {
        activeTintColor: 'orange',
        inactiveTintColor: 'orange',
        inactiveTintColor : 'grey',
        
        labelStyle : {
            fontSize : 12
        }          
    } 

    

});

const FiltersNavigator = createStackNavigator({
    Filters : FiltersScreen
});

const MainNavigator = createDrawerNavigator({
    'Meals': MealsFavTabNavigator,
    Filters: FiltersNavigator
},{
    contentOptions : {
        activeTintColor : Colors.accentColor,        
    }
});

export default createAppContainer(MainNavigator);